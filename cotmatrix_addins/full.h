#ifndef IGL_FULL_H
#define IGL_FULL_H
#include "igl_inline.h"
#define EIGEN_YES_I_KNOW_SPARSE_MODULE_IS_NOT_STABLE_YET
#include <Eigen/Dense>
#include <Eigen/Sparse>
namespace igl
{
  // Convert a sparsematrix into a full one
  // Templates:
  //   T  should be a eigen sparse matrix primitive type like int or double
  // Input:
  //   A  m by n sparse matrix
  // Output:
  //   B  m by n dense/full matrix
  template <typename T>
  IGL_INLINE void full(
    const Eigen::SparseMatrix<T> & A,
    Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic> & B);
}

#ifdef IGL_HEADER_ONLY
#  include "full.cpp"
#endif

#endif

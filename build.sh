#!/bin/bash

DEBUG=false
RELEASE=false

if [ $# == 0 ]
then
	RELEASE=true
else

	while [[ $# > 0 ]]
	do
	key="$1"

	case $key in
		-d|--debug)
			DEBUG=true
		;;
		-r|--release)
			RELEASE=true
		;;
		*)
			echo "Unknown option "$1
		;;
	esac
	shift
	done;
fi


if $DEBUG
then
	make clean
	time make debug
fi

if $RELEASE
then
	make clean
	time make all
fi

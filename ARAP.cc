#include "ARAP.hh"
#include "patch.hh"
#include "cotmatrix_addins/cotangent.h"
#include "cotmatrix_addins/cotmatrix.h"
#include <iostream>
#include <Eigen/Geometry>
#include <math.h>
ARAP::ARAP( VF& m ) : mesh(m)
{
	set_mesh( m );
	done_iterations = 0;
}

void ARAP::extract_boundary()
{
	std::vector<E_t> E;//, B;
	std::vector<int> Ib(mesh.V.rows(), -1);
	MatrixXi &F = mesh.F;

	for (unsigned i=0; i<F.rows(); i++)
	{
		E.push_back(E_t(F(i,0),F(i,1)));
		E.push_back(E_t(F(i,1),F(i,2)));
		E.push_back(E_t(F(i,2),F(i,0)));
	}
	std::sort(E.begin(), E.end(), E_t::are_ordered);

	#if 0
	for(std::vector<E_t>::iterator it = E.begin(); it!=E.end(); it++)
	{
		std::cout << (*it).v1 << "\t" << (*it).v2 << "\t" << (*it).inversed << std::endl;
	}
	#endif

	for(std::vector<E_t>::iterator it = E.begin(); it!=E.end(); it++)
	{
		E_t& e1=*it, e2=*(it+1);
		//std::cout<<"e1( "<<e1.v1<<" "<<e1.v2<<" )"<<std::endl;
		//std::cout<<"e2( "<<e2.v1<<" "<<e2.v2<<" )"<<std::endl;
		if(e1.v1 == e2.v1 && e1.v2 == e2.v2)
		{
			it++;
		} else
		{
			if(e1.inversed)
			{
				std::swap(e1.v1, e1.v2);
				e1.inversed = false;
			}
			//std::cout<<"B.push_back( "<<e1.v1<<" "<<e1.v2<<" )"<<std::endl;
			B.push_back(e1);
			Ib[e1.v1] = e1.v2;
		}
		//std::cout<<std::endl;
	}

	Vb.push_back(B[0].v1);
	//std::cout<<"pushing "<<B[0].v1<<std::endl;
	for (int i = B[0].v2; i != B[0].v1; i=Ib[i])
	{
		//std::cout<<"pushing "<<i<<std::endl;
		Vb.push_back(i);
	}
}

const std::vector<int>& ARAP::get_boundary_ids() const
{
	return Vb;
}
void ARAP::set_mesh( VF& mesh )
{
	this->mesh = mesh;
	Vb.clear();
	R.resize( mesh.F.rows() );
}
void ARAP::compute_mesh_area()
{
	int faces_count = mesh.F.rows();
	mesh_area = 0.0f;
	for ( int i=0; i!= faces_count; ++i )
	{
		Vector3d v0 =
		   Vector3d( mesh.V( mesh.F( i, 0 ), 0 ),
		             mesh.V( mesh.F( i, 0 ), 1 ),
		             mesh.V( mesh.F( i, 0 ), 2 ) );
		Vector3d v1 =
		   Vector3d( mesh.V( mesh.F( i, 1 ), 0 ),
		             mesh.V( mesh.F( i, 1 ), 1 ),
		             mesh.V( mesh.F( i, 1 ), 2 ) );
		Vector3d v2 =
		   Vector3d( mesh.V( mesh.F( i, 2 ), 0 ),
		             mesh.V( mesh.F( i, 2 ), 1 ),
		             mesh.V( mesh.F( i, 2 ), 2 ) );

		Vector3d v10 = v0 - v1;
		Vector3d v12 = v2 - v1;
		double n = (v10.cross( v12 )).norm();
		mesh_area += 0.5 * n;
	}
}
void ARAP::compute_boundary_2d_polygon()
{
	parametrization_radius = sqrt( mesh_area / M_PI );
	double boundary_length = 0.0;
	double partial_length = 0.0;
	boundary_polygon_2d.resize( Vb.size() );
	VP = MatrixXd( mesh.V.rows(), 2 );
	for ( int i = 0; i!=VP.rows(); ++i )
	{
		VP(i,0) = 0.0;
		VP(i,1) = 0.0;
	}

	for ( int i = 0; i!=Vb.size(); ++i )
	{
		int next_i = i == Vb.size() - 1 ? 0 : i + 1;
		Vector3d v0 = Vector3d(
		   mesh.V( Vb[i], 0 ),
		   mesh.V( Vb[i], 1 ),
		   mesh.V( Vb[i], 2 ) );
		Vector3d v1 = Vector3d(
		   mesh.V( Vb[next_i], 0 ),
		   mesh.V( Vb[next_i], 1 ),
		   mesh.V( Vb[next_i], 2 ) );
		boundary_length += (v1-v0).norm();
	}
	for ( int i = 0; i!=Vb.size(); ++i )
	{
		int next_i = i == Vb.size() - 1 ? 0 : i + 1;
		Vector3d v0 = Vector3d(
		   mesh.V( Vb[i], 0 ),
		   mesh.V( Vb[i], 1 ),
		   mesh.V( Vb[i], 2 ) );
		Vector3d v1 = Vector3d(
		   mesh.V( Vb[next_i], 0 ),
		   mesh.V( Vb[next_i], 1 ),
		   mesh.V( Vb[next_i], 2 ) );
		double a = 2.0 *  M_PI * ( partial_length / boundary_length );
		Vector2d p;
		p(0) = cos( a ) * parametrization_radius;
		p(1) = sin( a ) * parametrization_radius;
		VP(Vb[i], 0) = p(0);
		VP(Vb[i], 1) = p(1);
		boundary_polygon_2d[i] = p;
		double le = (v1-v0).norm();
		partial_length += le;
	}
}

void ARAP::fix_all_boundary_vertices()
{
	fixed_vertices_ids = Vb;
}
void ARAP::fix_2_vertices( int v0, int v1 )
{
	assert( v0 >=0 );
	assert( v1 >=0 );
	assert( v0 < mesh.V.rows() );
	assert( v1 < mesh.V.rows() );

	fixed_vertices_ids.resize( 2 );
	fixed_vertices_ids[0] = v0;
	fixed_vertices_ids[1] = v1;
}

void ARAP::fix_first_and_middle_boundary_vertices()
{
	int v0 = Vb[0];
	int v1 = -1;
	Vector2d target_point;
	target_point(0) = -parametrization_radius;
	target_point(1) = 0.0;
	double min_dist = parametrization_radius;
	for ( int i = 0; i!=Vb.size(); ++i )
	{
		Vector2d v;
		v(0) = VP(i,0);
		v(1) = VP(i,1);
		double dist = (v-target_point).norm();
		if ( dist < min_dist )
		{
			min_dist = dist;
			v1 = i;
		}
	}
	fix_2_vertices( v0, v1 );
}

void ARAP::rearrange_vertices_order()
{
	std::vector<unsigned char> id_is_fixed(mesh.V.rows());
	for ( int i = 0; i!= id_is_fixed.size(); ++i )
	{
		id_is_fixed[i] = 0;
	}
	for ( int i = 0; i!= fixed_vertices_ids.size(); ++i )
	{
		id_is_fixed[fixed_vertices_ids[i]] = 1;
	}
	std::vector<int> remapped_ids(mesh.V.rows());
	std::vector<int> inverse_remap(mesh.V.rows());
	for ( int t = 0, r = 0; t!=2; ++t )
	{
		for ( int i = 0; i!=id_is_fixed.size(); ++i )
		{
			if ( id_is_fixed[i] == t )
			{
				remapped_ids[r] = i;
				inverse_remap[i] = r;
				++r;
			}
		}
	}
	{
		MatrixXd original_V(mesh.V.rows(),3);
		int nv = mesh.V.rows();
		for ( int i = 0; i!=nv; ++i )
		{
			for ( int j = 0; j != 3; ++j )
			{
				original_V(i,j) = mesh.V(i,j);
			}
		}
		for ( int i = 0; i!=nv; ++i )
		{
			for ( int j = 0; j != 3; ++j )
			{
				mesh.V(i,j) = original_V( remapped_ids[i], j );
			}
		}
	}
	{
		MatrixXd original_VP(VP.rows(),2);
		int nvp = VP.rows();
		for ( int i = 0; i!=nvp; ++i )
		{
			for ( int j = 0; j != 2; ++j )
			{
				original_VP(i,j) = VP(i,j);
			}
		}
		for ( int i = 0; i!=nvp; ++i )
		{
			for ( int j = 0; j != 2; ++j )
			{
				VP(i,j) = original_VP( remapped_ids[i], j );
			}
		}
	}

	int nf = mesh.F.rows();
	for ( int i = 0; i!=nf; ++i )
	{
		for ( int j = 0; j != 3; ++j )
		{
			int id = mesh.F(i,j);
			mesh.F(i,j) = inverse_remap[id];
		}
	}
	// NOTE(theGiallo): IV is never used, here we should remap it too
	mesh.initTT();

	int nb = Vb.size();
	for ( int i = 0; i!=nb; ++i )
	{
		Vb[i] = inverse_remap[Vb[i]];
	}
	nb = fixed_vertices_ids.size();
	for ( int i = 0; i!=nb; ++i )
	{
		fixed_vertices_ids[i] = inverse_remap[fixed_vertices_ids[i]];
	}
	nb = B.size();
	for ( int i = 0; i!=nb; ++i )
	{
		B[i].v1 = inverse_remap[B[i].v1];
		B[i].v2 = inverse_remap[B[i].v2];
	}
}


void ARAP::compute_parametrization()
{
	Patch patch( mesh.V, mesh.F, VP, Vb.size(), mesh.V.rows()-Vb.size() );
	MatrixXd par = patch.computeHarmonicMap();
#if 0
	printf( "par rows = %d\n", par.rows() );
	printf( "par cols = %d\n", par.cols() );

	int nvp = par.rows();
	for ( int i = 0; i!=nvp; ++i )
	{
		printf( "par[%d] = %f, %f\n",
		        i, par(i,0), par(i,1) );
	}
#endif

	VP = par;
}

void ARAP::compute_cotangents()
{
	igl::cotangent( mesh.V, mesh.F, C );
}

void ARAP::compute_triangle_in_2d_plane_centered_in_origin(
   int triangle_id, Vector2d * out_triangle_2d_vertices )
{
	Vector3i tr_ids;
	for ( int j = 0; j != 3; ++j )
	{
		tr_ids(j) = mesh.F( triangle_id, j );
	}

	Vector3d t[3], ot[3];
	for ( int j = 0; j!=3; ++j )
	{
		for ( int k = 0; k!=3; ++k )
		{
			t[j](k) = mesh.V( tr_ids( j ), k );
		}
	}
	for ( int j = 0; j!=3; ++j )
	{
		ot[j] = t[j] - t[0];
	}

	// NOTE(theGiallo): this is all 0s
	t[0] = ot[0];

	t[1](0) = ot[1].norm();
	t[1](1) = 0.0;
	t[1](2) = 0.0;

	// NOTE(theGiallo): here I could employ a matrix that performs a change
	// of basis, but the computation would be the same
	Vector3d X = ot[1].normalized();
	double x2 = ot[2].dot( X );
	Vector3d Y2 = ot[2] - ( X * x2 );
	double y2 = Y2.norm();
	t[2](0) = x2;
	t[2](1) = y2;
	t[2](2) = 0.0;

	Vector2d barycenter_t;
	barycenter_t(0) =
	barycenter_t(1) = 0.0;
	assert( barycenter_t(0) == 0.0 );
	assert( barycenter_t(1) == 0.0 );
	for ( int j = 0; j != 3; ++j )
	{
		for ( int k = 0; k!=2; ++k )
		{
			barycenter_t(k) += t[j](k);
		}
	}
	barycenter_t /= 3.0;

	for ( int j = 0; j != 3; ++j )
	{
		out_triangle_2d_vertices[j](0) = t[j](0) - barycenter_t(0);
		out_triangle_2d_vertices[j](1) = t[j](1) - barycenter_t(1);
	}
}
void ARAP::compute_rotation_matrices()
{
	int nt = mesh.F.rows();
	for ( int i=0; i!=nt; ++i )
	{
		Vector2d tr[3];
		compute_triangle_in_2d_plane_centered_in_origin( i, tr );

		Vector2d tr_p[3];
		for ( int j = 0; j != 3; ++j )
		{
			for ( int k = 0; k!=2; ++k )
			{
				int v_id = mesh.F( i, j );
				tr_p[j](k) = VP( v_id, k );
			}
		}

		MatrixXd P(2,3), Pp(2,3), D(3,3);
		for ( int j = 0; j != 3; ++j )
		{
			int prev_j = j == 0 ? 2 : j - 1;
			int next_j = j == 2 ? 0 : j + 1;
			for ( int k = 0; k!=2; ++k )
			{
				#if S_AS_IN_SLIDES
				P (k,j) = tr  [next_j](k) - tr  [prev_j](k);
				Pp(k,j) = tr_p[next_j](k) - tr_p[prev_j](k);
				#else
				P (k,j) = tr  [j](k) - tr  [next_j](k);
				Pp(k,j) = tr_p[j](k) - tr_p[next_j](k);
				#endif
			}
			D(j,prev_j) = D(j,next_j) =  0.0;
			#if D_AS_IN_SLIDES
			D(j,j) = C(i,j);
			#else
			D(j,j) = C(i,prev_j);
			#endif
		}
		// NOTE(theGiallo): in the paper is P * D * Pp.transpose()
		#define AS_IN_PAPER 1
		#if AS_IN_PAPER
		MatrixXd S = P * D * Pp.transpose();
		#else
		MatrixXd S = Pp * D * P.transpose();
		#endif
		JacobiSVD<MatrixXd> svd( S, ComputeFullU | ComputeFullV );
		MatrixXd U = svd.matrixU();
		MatrixXd V = svd.matrixV();
		// NOTE(theGiallo): in the paper is V * U.transpose()
		MatrixXd R;
		#if AS_IN_PAPER
		R = V * U.transpose();
		#else
		R = U * V.transpose();
		#endif

		double det = R.determinant();
		while ( det < 0.0 )
		{
			for ( int j = 0; j != U.rows(); ++j )
			{
				U( j, U.cols() - 1 ) *= -1.0;
			}
			// TODO(theGiallo, 2017-01-23): I inverted U and V, check if it's correct
			// in the paper is V * U.transpose()
			#if AS_IN_PAPER
			R = V * U.transpose();
			#else
			R = U * V.transpose();
			#endif
			det = R.determinant();
		}
		this->R[i] = R;
	}
}

static
SparseMatrix<double>
sparse_block( SparseMatrix<double,ColMajor> M,
              int ibegin, int jbegin, int icount, int jcount )
{
	assert( ibegin + icount <= M.rows() );
	assert( jbegin + jcount <= M.cols() );
	int Mj, Mi, i, curr_outer_index, next_outer_index;
	std::vector<Triplet<double>> triplet_list;
	triplet_list.reserve( M.nonZeros() );

	for ( int j = 0; j < jcount; ++j )
	{
		Mj = j + jbegin;
		curr_outer_index = M.outerIndexPtr()[Mj    ];
		next_outer_index = M.outerIndexPtr()[Mj + 1];

		for ( int a = curr_outer_index; a < next_outer_index; a++ )
		{
			Mi = M.innerIndexPtr()[a];

			if ( Mi < ibegin )
			{
				continue;
			}
			if ( Mi >= ibegin + icount )
			{
				break;
			}

			i = Mi - ibegin;
			triplet_list.push_back( Triplet<double>( i, j, M.valuePtr()[a] ) );
		}
	}
	SparseMatrix<double> ret( icount, jcount );
	ret.setFromTriplets( triplet_list.begin(), triplet_list.end() );
	return ret;
}

void ARAP::build_laplacian_system()
{
	SparseMatrix<double> L;
	igl::cotmatrix( mesh.V, mesh.F, L );

	int c = fixed_vertices_ids.size();
	int f = mesh.V.rows() - c;

	SparseMatrix<double> Lff;
	Lff = sparse_block( L, 0, 0, f, f );
	SparseMatrix<double> Lfc = sparse_block( L, 0, f, f, c );

	assert( Lff.rows() == f );
	assert( Lfc.rows() == f );
	assert( Lff.cols() == f );
	assert( Lfc.cols() == c );

#if 0
	// NOTE(theGiallo): checked: submatrices are correct ( it took hours )
	printf( "\nLff: %d, %d\n", Lff.rows(), Lff.cols() );
	printf( "Lfc: %d, %d\n", Lfc.rows(), Lfc.cols() );
	printf( "going to check Lff\n" );
	fflush( stdout );

	for ( int r = 0; r != Lff.rows(); ++r )
	{
		printf( "   row %d\n", r );
		for ( int c = 0; c != Lff.cols(); ++c )
		{
			assert( Lff.coeffRef(r,c) == L.coeffRef(r,c) );
		}
	}
	printf( "going to check Lfc\n" );
	fflush( stdout );
	for ( int r = 0; r != Lfc.rows(); ++r )
	{
		printf( "   row %d\n", r );
		for ( int c = 0; c != Lfc.cols(); ++c )
		{
			assert( Lfc.coeffRef(r,c) == L.coeffRef(r,f+c) );
		}
	}

	printf( "Checks done. Submatrices are correct.\n" );
	fflush( stdout );
#endif

#if 0
	// NOTE(theGiallo): checked manually: correct Lff sub-block
	printf( "L:\n" );
	for ( int r = 0; r != L.rows(); ++r )
	{
		for ( int c = 0; c != L.cols(); ++c )
		{
			printf( "%6.2f ", L.coeffRef(r,c) );
		}
		printf( "\n" );
	}
	printf( "\n" );


	printf( "Lff:\n" );
	for ( int r = 0; r != Lff.rows(); ++r )
	{
		for ( int c = 0; c != Lff.cols(); ++c )
		{
			printf( "%6.2f ", Lff.coeffRef(r,c) );
		}
		printf( "\n" );
	}
	printf( "\n" );
#endif

#if 0
	MatrixXd Vc(c,2);
	for ( int i = 0; i != c; ++i )
	{
		Vc(i, 0) = VP( f + i, 0 );
		Vc(i, 1) = VP( f + i, 1 );
	}
#else
	MatrixXd Vc = VP.block( f, 0, c, 2 );
#endif

#if 1
	LfcVc.resize( Lfc.rows(), 2 );
	LfcVc.reserve( Lfc.rows() );
#endif
	LfcVc = Lfc * Vc;

#if FUCK
#error
	// NOTE(theGiallo): is this the same?
	chol.analyzePattern( Lff );
	chol.factorize( Lff );
#else
	chol.compute( Lff );
#endif
}

void ARAP::optimize_vertices()
{
	int c = fixed_vertices_ids.size();
	int f = mesh.V.rows() - c;
	MatrixXd bf_u(f,1);
	MatrixXd bf_v(f,1);

	for ( int i = 0; i != f; ++i )
	{
		bf_u(i) = 0.0;
		bf_v(i) = 0.0;
	}

	int nt = mesh.F.rows();
	for ( int t_id = 0; t_id != nt; ++t_id )
	{
		Vector3i triangle_vertices_ids;
		triangle_vertices_ids(0) = mesh.F(t_id,0);
		triangle_vertices_ids(1) = mesh.F(t_id,1);
		triangle_vertices_ids(2) = mesh.F(t_id,2);
		if ( triangle_vertices_ids(0) >= f &&
		     triangle_vertices_ids(1) >= f &&
		     triangle_vertices_ids(2) >= f )
		{
			continue;
		}
		Vector2d triangle_2d[3];
		compute_triangle_in_2d_plane_centered_in_origin( t_id, triangle_2d );

		Vector3i TT_triangle_ids( mesh.getTT( t_id ) );
		for ( int i = 0; i != 3; ++i )
		{
			int triangle_vertex_id_02        = i;
			int triangle_vertex_id_02_after  = i == 2 ? 0 : i + 1;
			int triangle_vertex_id_02_before = i == 0 ? 2 : i - 1;
			int triangle_vertex_id = triangle_vertices_ids(triangle_vertex_id_02);
			if ( triangle_vertex_id >= f )
			{
				continue;
			}

			int triangle_vertex_id_after = triangle_vertices_ids(triangle_vertex_id_02_after);
			int adj_tr_id = TT_triangle_ids(triangle_vertex_id_02);
			Vector2d db;
			if ( adj_tr_id >= 0 )
			{
				int adjacent_triangle_vertex_id_02_opposite = -1;
				Vector3i adjacent_triangle_vertices_ids;
				adjacent_triangle_vertices_ids(0) = 0;
				int xxx   = mesh.F( adj_tr_id, 0 );
				adjacent_triangle_vertices_ids(0) = xxx;
				adjacent_triangle_vertices_ids(1) = mesh.F( adj_tr_id, 1 );
				adjacent_triangle_vertices_ids(2) = mesh.F( adj_tr_id, 2 );
				for ( int k = 0; k != 3; ++k )
				{
					int adjacent_triangle_vertex_id_02_after  = k == 2 ? 0 : k + 1;
					int adjacent_triangle_vertex_id_02_before = k == 0 ? 2 : k - 1;
					if ( adjacent_triangle_vertices_ids(adjacent_triangle_vertex_id_02_after ) == triangle_vertex_id_after &&
					     adjacent_triangle_vertices_ids(adjacent_triangle_vertex_id_02_before) == triangle_vertex_id )
					{
						adjacent_triangle_vertex_id_02_opposite = k;
					}
				}
				assert( adjacent_triangle_vertex_id_02_opposite != -1 );
				db =
				   ( C( t_id,      triangle_vertex_id_02_before            ) * R[ t_id      ] +
				     C( adj_tr_id, adjacent_triangle_vertex_id_02_opposite ) * R[ adj_tr_id ]   ) *
				#define BF_AS_IN_SLIDES 0
				#if BF_AS_IN_SLIDES
				   ( triangle_2d[triangle_vertex_id_02_after] - triangle_2d[triangle_vertex_id_02] ) * 0.5;
				#else
				   ( triangle_2d[triangle_vertex_id_02] - triangle_2d[triangle_vertex_id_02_after] ) * 0.5;
				#endif
			} else
			{
				#if BF_AS_IN_SLIDES
				db =
				   ( C( t_id, triangle_vertex_id_02_before ) * R[ t_id  ] ) * ( triangle_2d[triangle_vertex_id_02_after] - triangle_2d[triangle_vertex_id_02] ) * 0.5;
				#else
				   ( C( t_id, triangle_vertex_id_02_before ) * R[ t_id  ] ) * ( triangle_2d[triangle_vertex_id_02] - triangle_2d[triangle_vertex_id_02_after] ) * 0.5;
				#endif

				if ( triangle_vertex_id_after < f )
				{
					Vector2d dbj;
				#if BF_AS_IN_SLIDES
					dbj =
					   ( C( t_id, triangle_vertex_id_02_before ) * R[ t_id  ] ) * ( triangle_2d[triangle_vertex_id_02] - triangle_2d[triangle_vertex_id_02_after] ) * 0.5;
				#else
					   ( C( t_id, triangle_vertex_id_02_before ) * R[ t_id  ] ) * ( triangle_2d[triangle_vertex_id_02_after] - triangle_2d[triangle_vertex_id_02] ) * 0.5;
				#endif
					bf_u(triangle_vertex_id_after) += dbj(0);
					bf_v(triangle_vertex_id_after) += dbj(1);
				}
			}
			bf_u(triangle_vertex_id) += db(0);
			bf_v(triangle_vertex_id) += db(1);
		}
	}

#if 0
	printf( "LfcVc size: %dx%d\n", LfcVc.rows(), LfcVc.cols() );
	printf( "LfcVc.col(0) size: %dx%d\n", LfcVc.col(0).rows(), LfcVc.col(0).cols() );
	printf( "LfcVc.col(1) size: %dx%d\n", LfcVc.col(1).rows(), LfcVc.col(1).cols() );
	printf( "bf_u size: %dx%d\n", bf_u.rows(), bf_u.cols() );
	printf( "bf_v size: %dx%d\n", bf_v.rows(), bf_v.cols() );

	for ( int i = 0; i!=LfcVc.outerSize(); ++i )
	{
		for ( SparseMatrix<double>::InnerIterator it( LfcVc, i );
		      it;
		      ++it )
		{
			if ( i == 0 )
				bf_u( it.row() ) -= it.value();
			else
				bf_v( it.row() ) -= it.value();
		}
	}
#else
	for ( int i = 0; i != 2; ++i )
	{
		if ( i == 0 )
		{
			bf_u -= LfcVc.col( i );
		} else
		{
			bf_v -= LfcVc.col( i );
		}
	}
#endif

	for ( int d = 0; d != 2; ++d )
	{
		MatrixXd xf =
		   chol.solve( d==0?bf_u:bf_v );
		for ( int i = 0; i != f; ++i )
		{
			VP(i,d) = xf(i);
		}
	}
}

void ARAP::iteration_step( Fixed_Type ft )
{
	compute_rotation_matrices();

	if ( !done_iterations )
	{
		switch ( ft )
		{
			case FIX_ALL_BOUNDARY:
				fix_all_boundary_vertices();
				break;
			case FIX_2_ON_BOUNDARY:
				fix_first_and_middle_boundary_vertices();
				break;
			default:
				break;
		}
		rearrange_vertices_order();
		build_laplacian_system();
	}

	optimize_vertices();

	++done_iterations;
}

double ARAP::compute_parametrization_energy()
{
	double ret = 0.0;

	int nt = mesh.F.rows();
	for ( int t = 0; t != nt; ++t )
	{
		Vector3i tids;
		tids(0) = mesh.F(t,0);
		tids(1) = mesh.F(t,1);
		tids(2) = mesh.F(t,2);
		Vector2d tr[3];
		compute_triangle_in_2d_plane_centered_in_origin( t, tr );

		Vector2d tr_p[3];
		for ( int j = 0; j != 3; ++j )
		{
			for ( int k = 0; k!=2; ++k )
			{
				tr_p[j](k) = VP( tids[j], k );
			}
		}

		for ( int i = 0; i!=3; ++i )
		{
			int next_i = i == 2 ? 0 : i + 1;
			int prev_i = i == 0 ? 2 : i - 1;
			ret +=
			   C( t, prev_i ) *
			   ( tr_p[i] - tr_p[next_i] -
			     R[t] * ( tr[i] - tr[next_i] ) ).squaredNorm();
		}
	}

	return ret;
}

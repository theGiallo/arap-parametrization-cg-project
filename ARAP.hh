#ifndef ARAP_H
#define ARAP_H
#include "VF.hh"
#include "cotmatrix_addins/cotmatrix.h"
#include <vector>
#include <algorithm>

class ARAP
{
private:
public:
	std::vector<int> Vb;
	std::vector<int> fixed_vertices_ids;
	std::vector<Vector2d> boundary_polygon_2d;
	VF& mesh;
	double mesh_area;
	double parametrization_radius; // NOTE(theGiallo): radius of planar parametrization
	MatrixXd VP;   // NOTE(theGiallo): parametrization of every vertex
	MatrixXd C;    // NOTE(theGiallo): cotangent of every vertex in the mesh, for each triangle
	               // C(triangle_id, vertex_id_0_2)
	SparseMatrix<double> LfcVc; // NOTE(theGiallo): matrix used in the iteration for optimizing vertices
	//SparseMatrix<double> Lff;   // NOTE(theGiallo): matrix used in the iteration for optimizing vertices
	SimplicialCholesky<SparseMatrix<double>> chol;    // NOTE(theGiallo): build with Lff
	std::vector<MatrixXd> R;    // NOTE(theGiallo): rotation matrices for each triangle relating them to the parametrization
	typedef struct E_st
	{
		int v1, v2;
		bool inversed;
		E_st(int v1, int v2):v1(v1),v2(v2),inversed(false){order();};
		void order()
		{
			if(v1>v2)
			{
				std::swap(v1,v2);
				inversed = true;
			}
		}
		static bool are_ordered(struct E_st e1, struct E_st e2) //to be passed to std::sort
		{
			if(e1.v1 < e2.v1)
				return true;
			if(e1.v1 == e2.v1)
			{
				if (e1.v2 < e2.v2)
					return true;
				if (e1.v2 == e2.v2)
					return !e1.inversed && e2.inversed;
			}
			return false;
		}

	}E_t;

	enum
	Fixed_Type
	{
		FIX_ALL_BOUNDARY,
		FIX_2_ON_BOUNDARY,
		// ---
		FIXED_TYPES_COUNT
	};
	int done_iterations;

public:

	const std::vector<int>& get_boundary_ids() const;
	void extract_boundary();
	ARAP(VF& mesh);
	void set_mesh(VF& mesh);
	void compute_mesh_area();
	void compute_boundary_2d_polygon();
	void fix_all_boundary_vertices();
	void fix_first_and_middle_boundary_vertices();
	void fix_2_vertices( int v0, int v1 );
	void rearrange_vertices_order();
	void compute_parametrization();
	void compute_cotangents();
	void compute_triangle_in_2d_plane_centered_in_origin(
	   int triangle_id, Vector2d * out_triangle_2d_vertices );
	void compute_rotation_matrices();
	void build_laplacian_system();
	void optimize_vertices();

	void iteration_step( Fixed_Type ft );

	double compute_parametrization_energy();

	// TODO(theGiallo, 2017-01-09): the pdf says to discard this in the
	// extract_boundary
	std::vector<E_t> B;
};

#endif // ifndef ARAP_H

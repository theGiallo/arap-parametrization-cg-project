// opengl // #include <GL/glew.h>
#ifdef __APPLE__
#include <glut.h>
#else
#include <GL/glut.h>
#endif
#define __ESC_GLUT 27

#include <AntTweakBar.h>

#include "VF.hh"                    // VF data structure
#include "camera.hh"                // viewing controls
#include "patch.hh"   // harmonic parametrization
//#include "utility.hh"
//#include "GLutils.hh"
//#include "cotmatrix_addins/cotmatrix.h"
//#include "cotmatrix_addins/massmatrix.h"
//#include <Eigen/SparseCholesky>

#include "ARAP.hh"
#include <GL/freeglut.h> // la ho messa solo per avere le stampe grafiche di debug
#include <sstream>

/////////////
// GLOBALS //
/////////////

#define W 800
#define H 600

// OpenGl related vars
Camera camera;
int width = W;
int height = H;

VF::draw_mode_t mesh_draw_mode = VF::FLAT;

// Mesh:
VF m;
ARAP *arap;


ARAP::Fixed_Type arap_fixed_type = ARAP::FIX_ALL_BOUNDARY;
uint32_t arap_iterations_to_compute = 1;
uint32_t computed_iterations = 0;
uint32_t total_computed_iterations = 0;
bool computing_iterations = false;
double arap_energy = 0.0;
bool render_triangles_overlay = true;

void
arap_ui_step_callback( void * client_data )
{
	computed_iterations = 0;
	computing_iterations = true;
#if 0
	for ( uint32_t i=0; i!=arap_iterations_to_compute; ++i )
	{
		arap->iteration_step( arap_fixed_type );
	}
#endif
}


//////////
// QUIT //
//////////

void quit()
{
	// insert here eventual clean up code
	exit(0);
}


///////////////////
// GLUT CALLBACK //
///////////////////

static uint32_t frames_count = 0;
void idle_func()
{
	if ( computing_iterations )
	{
		printf( "[%4d] %s\n", frames_count, __PRETTY_FUNCTION__ );
		if ( computed_iterations < arap_iterations_to_compute )
		{
			printf( "    computing %u...", computed_iterations );
			arap->iteration_step( arap_fixed_type );
			++computed_iterations;
			++total_computed_iterations;
			printf( "    computed %u\n", computed_iterations );
			arap_energy = arap->compute_parametrization_energy();
		} else
		{
			computing_iterations = false;
		}
		glutPostRedisplay();
	}
}
void display()
{
	printf( "[%4d] %s\n", frames_count++, __PRETTY_FUNCTION__ );
	glClearColor(1.0f, 1.0f, 1.0f, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// draw mesh
	int wp_w = width/2;
	int wp_h = height;
	glViewport( 0, 0, (GLsizei) wp_w, (GLsizei) wp_h );
	camera.reshape( width/2,height);
	camera.display_begin();
	m.draw(mesh_draw_mode);

	// draw boundary on the mesh
	//glClear(GL_DEPTH_BUFFER_BIT);
	glDisable (GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glTranslatef (-m.center[0], -m.center[1], -m.center[2]);
	glPointSize(4.0);
	std::vector<ARAP::E_t> &B = arap->B;
	std::vector<int> &Vb = arap->Vb;

#if 0
	glColor3f (.8f,0.0f,0.0f);
	glBegin(GL_LINES);
	for (std::vector<ARAP::E_t>::iterator it = B.begin(); it!=B.end() ; it++)
	{
		glVertex3d(m.V((*it).v1,0),m.V((*it).v1,1),m.V((*it).v1,2));
		glVertex3d(m.V((*it).v2,0),m.V((*it).v2,1),m.V((*it).v2,2));
	}
	glEnd();
	glBegin(GL_POINTS);
	for (std::vector<ARAP::E_t>::iterator it = B.begin(); it!=B.end() ; it++)
	{
		glVertex3d(m.V((*it).v1,0),m.V((*it).v1,1),m.V((*it).v1,2));
		glVertex3d(m.V((*it).v2,0),m.V((*it).v2,1),m.V((*it).v2,2));
	}
	glEnd();
#else
	int n_vb = Vb.size();
	float green = 0.0f;
	float green_delta = 1.0f / (float)Vb.size();
	glColor3f (.8f,green,0.0f);
	glBegin(GL_LINES);
	for ( int i = 0; i != n_vb; ++i )
	{
		int next_i = ( i == ( n_vb - 1 ) ? 0 : i + 1 );
		int vi = Vb[i];
		int next_vi = Vb[next_i];
		green += green_delta;
		glVertex3d(m.V(vi,0),m.V(vi,1),m.V(vi,2));
		glColor3f (.8f,green,0.0f);
		glVertex3d(m.V(next_vi,0),m.V(next_vi,1),m.V(next_vi,2));
	}
	glEnd();
	green = 0.0f;
	glColor3f (.8f,green,0.0f);
	glBegin(GL_POINTS);
	for ( int i = 0; i != n_vb; ++i )
	{
		int vi = Vb[i];
		green += green_delta;
		glVertex3d(m.V(vi,0),m.V(vi,1),m.V(vi,2));
		glColor3f (.8f,green,0.0f);
	}
	glEnd();
#endif

	glPopMatrix();

	camera.display_end();


	// NOTE(theGiallo): right - 2d parametrization
	uint np;
	float wu_per_px = arap->parametrization_radius * 1.05f / (float)MIN(wp_w,wp_h);
	//std::cout<< "radius = " << arap->radius <<std::endl;
	glViewport( wp_w, 0, (GLsizei) wp_w, (GLsizei) wp_h );

	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D( -((float)wp_w) * wu_per_px, ((float)wp_w) * wu_per_px,
	            -((float)wp_h) * wu_per_px, ((float)wp_h) * wu_per_px );

	int nt = arap->mesh.F.rows();
	glBegin( GL_LINES );
	glColor3f( 0.71, 0.06f, 0.84f );
	for ( int i = 0; i != nt; ++i )
	{
		for ( int j = 0; j != 3; ++j )
		{
			int next_j = ( j == 2 ? 0 : j + 1 );
			glVertex2d( arap->VP( arap->mesh.F( i,      j ), 0 ),
			            arap->VP( arap->mesh.F( i,      j ), 1 ) );
			glVertex2d( arap->VP( arap->mesh.F( i, next_j ), 0 ),
			            arap->VP( arap->mesh.F( i, next_j ), 1 ) );
		}
	}
	glEnd();
	np = arap->VP.rows();
	glBegin( GL_POINTS );
	glColor3f( 0.15f, 0.71f, 0.11 );
	for ( int i = 0; i != np; ++i )
	{
		glVertex2d( arap->VP( i, 0 ),
		            arap->VP( i, 1 ) );
	}
	glEnd();

#if 0
	glBegin( GL_LINES );
	glColor3f( 0.176f, 0.78, 1.0f );
	std::vector<Vector2d> &p2d = arap->boundary_polygon_2d;
	uint np = p2d.size();
	for ( int i = 0; i != np; ++i )
	{
		int next_i = ( i == np - 1 ? 0 : i + 1 );
		glVertex2dv( (double*)(&p2d[i     ]) );
		glVertex2dv( (double*)(&p2d[next_i]) );
	}
	glEnd();

	glPointSize( 4.0f );
	glBegin( GL_POINTS );
	glColor3f( 1.0f, 0.59f, 0.176f );
	for ( int i = 0; i != np; ++i )
	{
		glVertex2dv( (double*)(&p2d[i     ]) );
	}
	glEnd();
#else
	glBegin( GL_LINES );
	//glColor3f( 0.176f, 0.78, 1.0f );
	green = 0.0f;
	glColor3f (.8f,green,0.0f);
	np = Vb.size();
	for ( int i = 0; i != np; ++i )
	{
		int next_i = ( i == ( np - 1 ) ? 0 : i + 1 );
		int vi = Vb[i];
		int next_vi = Vb[next_i];
		green += green_delta;
		glVertex2d(arap->VP(vi,0),arap->VP(vi,1));
		glColor3f (.8f,green,0.0f);
		glVertex2d(arap->VP(next_vi,0),arap->VP(next_vi,1));
	}
	glEnd();

	glPointSize( 4.0f );
	glBegin( GL_POINTS );
	green = 0.0f;
	glColor3f (.8f,green,0.0f);
	//glColor3f( 1.0f, 0.59f, 0.176f );
	for ( int i = 0; i != np; ++i )
	{
		int vi = Vb[i];
		green += green_delta;
		glVertex2d(arap->VP(vi,0),arap->VP(vi,1));
		glColor3f (.8f,green,0.0f);
	}
	glEnd();

#endif

	if ( render_triangles_overlay )
	{
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix();
		for ( int i = 0; i!=nt; ++i )
		{
			Vector2d tr[3];
			arap->compute_triangle_in_2d_plane_centered_in_origin( i, tr );

			Vector2d barycenter;
			barycenter(0) =
			barycenter(1) = 0.0;
			for ( int j = 0; j != 3; ++j )
			{
				for ( int k = 0; k!=2; ++k )
				{
					int tr_id = arap->mesh.F( i, j );
					barycenter(k) += arap->VP( tr_id, k );
				}
			}
			barycenter /= 3.0;

			glMatrixMode( GL_MODELVIEW );
			glLoadIdentity();

			glColor3f( 0.0f, 0.0f, 0.0f );
			glBegin( GL_POINTS );
			glVertex2d( barycenter(0),
			            barycenter(1) );
			glEnd();

			float scale = 1.0f;
			glTranslatef( barycenter(0), barycenter(1), 0.0f );
			if ( arap->R.size() && arap->R[0].rows() )
			{
				float rot[16] = {1,0,0,0,
				                 0,1,0,0,
				                 0,0,1,0,
				                 0,0,0,1};
				rot[0] = arap->R[i](0,0);
				rot[1] = arap->R[i](1,0);
				rot[4] = arap->R[i](0,1);
				rot[5] = arap->R[i](1,1);
				#if 0
				rot[10] = 1.0f;
				rot[15] = 1.0f;
				#endif
				glMultMatrixf( rot );
			}
			glScalef( scale, scale, 0.0f );

			for ( int j = 0; j!=3; ++j )
			{
				int next_j = j == 2 ? 0 : j + 1;
				glBegin( GL_LINES );
				glVertex2d( tr[j     ](0),
				            tr[j     ](1) );
				glVertex2d( tr[next_j](0),
				            tr[next_j](1) );
				glEnd();
			}
		}
		glMatrixMode( GL_MODELVIEW );
		glPopMatrix();
	}

	glPopMatrix();
	glMatrixMode( GL_MODELVIEW );
	glPopMatrix();

	// draw GUI
	TwDraw();

	glFlush();
	glutSwapBuffers();
}

void reshape(int w, int h)
{
	//camera.reshape(w,h);
	width = w;
	height = h;
	//glViewport(0, 0, (GLsizei) w, (GLsizei) h);
	TwWindowSize(w, h);
	glutPostRedisplay();
}


void mouse_click(int button, int state, int x, int y)
{
	if (TwEventMouseButtonGLUT(button, state, x, y))
	{
		glutPostRedisplay();
		return;
	}

	// insert here management of selection and editing

	// camera motion with left button
	camera.mouse(button, state, x, y);
	glutPostRedisplay();
}


void mouse_move(int x, int y)
{
	if (TwEventMouseMotionGLUT(x, y))
	{
		glutPostRedisplay();
		return;
	}

	// insert here management of selection and editing

	else // view motion
		camera.mouse_move(x,y);
	glutPostRedisplay();
}


void keyboard(unsigned char k, int x, int y)
{
	if (k == __ESC_GLUT) quit();

	TwEventKeyboardGLUT(k, x, y);

	// insert here management of other keybord keys

	//camera.key (k, x, y);
	glutPostRedisplay();
}

void special(int k, int x, int y)
{
	TwEventSpecialGLUT(k, x, y);

	//camera.special (k, x, y);
	glutPostRedisplay();
}


///////////////////////////
// ANTTWEAKBAR CALLBACKS //
///////////////////////////

void TW_CALL setAperture (const void *value, void *)
{
	camera.gCamera.aperture = *(const double *) value;
	glutPostRedisplay();
}

void TW_CALL getAperture (void *value, void *)
{
	*(double *) value = camera.gCamera.aperture;
}

void TW_CALL setFocalLength (const void *value, void *)
{
	camera.gCamera.focalLength = *(const double *) value;
	glutPostRedisplay();
}

void TW_CALL getFocalLength (void *value, void *)
{
	*(double *) value = camera.gCamera.focalLength;
}

void TW_CALL call_quit(void *clientData)
{
	quit();
}


//////////
// MAIN //
//////////

int main (int argc, char *argv[])
{
	if (argc != 2)
	{
	fprintf (stderr, "Usage: ./viewer mesh_file\n");
	exit (-1);
	}

	m.read(argv[1]);
	m.bb();

	arap = new ARAP(m);
	arap->extract_boundary();
	arap->compute_mesh_area();
	arap->compute_boundary_2d_polygon();
	arap->fix_all_boundary_vertices();
	arap->rearrange_vertices_order();
	arap->compute_parametrization();
	arap->compute_cotangents();
	arap->compute_rotation_matrices();
	arap_energy =
	arap->compute_parametrization_energy();
	arap->done_iterations = 0;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(W, H);
	glutCreateWindow("viewer");

	TwInit(TW_OPENGL, NULL);
	TwWindowSize(W, H);

	glClearColor(0, 0, 0, 0);
	glEnable(GL_NORMALIZE);
	glEnable(GL_COLOR_MATERIAL);
	//glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);

	camera.SetLighting(4);
	camera.gCameraReset(m.diagonal, m.center);

	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutMouseFunc(mouse_click);
	glutMotionFunc(mouse_move);
	glutPassiveMotionFunc(mouse_move);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(special);
	glutIdleFunc(idle_func);
	TwGLUTModifiersFunc(glutGetModifiers);

	TwBar *cBar;
	cBar = TwNewBar("Camera_Rendering");
	TwDefine("Camera_Rendering size='250 300'");
	TwDefine("Camera_Rendering valueswidth=80");
	TwDefine("Camera_Rendering color='192 255 192' text=dark ");

	TwAddButton(cBar, "Quit", call_quit, NULL, "");

	TwEnumVal draw_modeEV[VF_DRAW_MODE_NUM] = {
	   { VF::FLAT, "Flat" }, { VF::POINTS, "Points" }, { VF::WIRE, "Wire" } };
	   // { VF::HIDDEN, "Hidden" }, { VF::FLAT, "Flat" }};
	TwType draw_modeT = TwDefineEnum("drawModeType", draw_modeEV, VF_DRAW_MODE_NUM);
	TwAddVarRW(cBar, "DrawMode", draw_modeT, &mesh_draw_mode,
	           "group = 'Scene'" " keyIncr='<' keyDecr='>'");

	// info
	TwAddVarRW(cBar, "show camera help", TW_TYPE_BOOLCPP, &camera.gShowHelp, "group = 'Scene'");

	// help
	TwAddVarRW(cBar, "show camera info", TW_TYPE_BOOLCPP, &camera.gShowInfo, "group = 'Scene'");


#define STR_GROUP_ARAP " group = 'ARAP' "
	TwEnumVal arap_fixed_types_ev[ARAP::FIXED_TYPES_COUNT] = {
	   { ARAP::FIX_ALL_BOUNDARY,  "all boundary"  },
	   { ARAP::FIX_2_ON_BOUNDARY, "2 on boundary" }, };
	TwType arap_fixed_types_t =
	   TwDefineEnum( "arap_fixed_types_t", arap_fixed_types_ev,
	                 ARAP::FIXED_TYPES_COUNT );
	TwAddVarRW( cBar, "Fixed vertices", arap_fixed_types_t, &arap_fixed_type,
	            STR_GROUP_ARAP );
	int res =
	   TwAddButton( cBar, "step", &arap_ui_step_callback, NULL,
	                STR_GROUP_ARAP
	                " label='step' help='compute a cycle of the ARAP iteration'" );

	res =
	TwAddVarRW( cBar, "iterations number", TW_TYPE_UINT32,
	            &arap_iterations_to_compute,
	            STR_GROUP_ARAP
	            "min=1" );
	res =
	TwAddVarRO( cBar, "computed iterations", TW_TYPE_UINT32,
	            &computed_iterations,
	            STR_GROUP_ARAP );

	res =
	TwAddVarRO( cBar, "total computed iterations", TW_TYPE_UINT32,
	            &total_computed_iterations,
	            STR_GROUP_ARAP );

	res =
	TwAddVarRO( cBar, "ARAP energy", TW_TYPE_DOUBLE,
	            &arap_energy,
	            STR_GROUP_ARAP );

	res =
	TwAddVarRW( cBar, "render triangles overlay", TW_TYPE_BOOLCPP,
	            &render_triangles_overlay,
	            STR_GROUP_ARAP );


	// aperture
	TwAddVarCB(cBar, "camera aperture", TW_TYPE_DOUBLE, setAperture, getAperture,
	           NULL, "min=0.00 max=100.00 step=0.1");

	// focus distance
	TwAddVarCB(cBar, "camera focus", TW_TYPE_DOUBLE, setFocalLength, getFocalLength,
		   NULL, "min=0.00 max=100.00 step=0.1");

	glutMainLoop();
	delete arap;
	exit (-1);
}
